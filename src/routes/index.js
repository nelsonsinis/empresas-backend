const {
  Router,
} = require('express');
const {
  signIn,
  getEnterpriseIndex,
  getEnterprise,
} = require('../controllers');

const router = Router();

router.post('/users/auth/sign_in', signIn);
router.get('/enterprises', getEnterpriseIndex);
router.get('/enterprises/:id', getEnterprise);

module.exports = router;
