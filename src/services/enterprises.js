const {
  getEnterpriseIndex,
  getEnterprise,
} = require('../repositories');

module.exports = {
  getEnterpriseIndex(enterpriseTypes, name) {
    return getEnterpriseIndex(enterpriseTypes, name);
  },
  getEnterprise(id) {
    return getEnterprise(id);
  },
};
