const sha1 = require('sha1');
const {
  sign,
} = require('jsonwebtoken');
const {
  clientId,
} = require('../config/config.json');
const {
  getUser,
} = require('../repositories');

module.exports = {
  signIn(email, password) {
    return new Promise(async (resolve, reject) => {
      try {
        let user = await getUser(email);

        if (!user) {
          reject({ // eslint-disable-line
            error: 'User not found',
          });
        }

        const hashPassword = sha1(password);

        if (hashPassword !== user.password) {
          reject({ // eslint-disable-line
            error: 'Email/password are incorrects',
          });
        }

        sign({
          email,
          name: user.investor_name,
          id: user.id,
        }, clientId, {
          expiresIn: '1h',
        }, (error, decoded) => {
          if (error) {
            reject({ // eslint-disable-line
              error,
            });
          }

          user = Object.assign({}, user._doc);
          delete user.password;

          resolve({
            clientId,
            user,
            token: decoded,
          });
        });
      } catch (error) {
        reject(error);
      }
    });
  },
};
