const mongoose = require('mongoose');
const {
  enterprises: enterprisesSchema,
} = require('../schemas');

module.exports = {
  getEnterpriseIndex(enterpriseTypes, name) {
    const model = mongoose.model('Enterprise', enterprisesSchema);
    return model.findOne({
      'enterprise_type.id': enterpriseTypes,
      enterprise_name: new RegExp(name, 'i'),
    });
  },
  getEnterprise(id) {
    const model = mongoose.model('Enterprise', enterprisesSchema);
    return model.findOne({
      id,
    });
  },
};
