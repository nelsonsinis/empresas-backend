const mongoose = require('mongoose');
const {
  users
} = require('../schemas');

module.exports = {
  getUser(email) {
    const model = mongoose.model('User', users);
    return model.findOne({
      email,
    }, {
      _id: false,
    });
  },
};
