const auth = require('./auth');
const enterprises = require('./enterprises');

module.exports = {
  ...auth,
  ...enterprises,
};
