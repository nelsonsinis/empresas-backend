const {
  getEnterpriseIndex,
  getEnterprise,
} = require('../services');

module.exports = {
  async getEnterpriseIndex(req, res) {
    const {
      enterprise_types: enterpriseTypes,
      name,
    } = req.query;

    try {
      const response = await getEnterpriseIndex(enterpriseTypes, name);

      res.header('access-token', req.headers['access-token']); // eslint-disable-line
      res.header('client', req.headers.client);
      res.header('uid', req.headers.uid);
      res.status(200).json(response);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  async getEnterprise(req, res) {
    const {
      id,
    } = req.params;

    try {
      const response = await getEnterprise(id);

      res.header('access-token', req.headers['access-token']); // eslint-disable-line
      res.header('client', req.header.client);
      res.header('uid', req.headers.uid);
      res.status(200).json(response);
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
