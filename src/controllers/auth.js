const {
  signIn,
} = require('../services');

module.exports = {
  async signIn(req, res) {
    try {
      const {
        email,
        password,
      } = req.body;

      const {
        clientId,
        token,
        user,
      } = await signIn(email, password);

      delete user.password;

      res.header('access-token', token);
      res.header('client', clientId);
      res.header('uid', email);

      res.status(200).json(user);
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
