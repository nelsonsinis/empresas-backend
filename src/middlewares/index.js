const {
  verify,
} = require('jsonwebtoken');
const {
  clientId,
} = require('../config/config.json');

const validate = (token, client, uid) => new Promise((resolve, reject) => {
  if (!token || !client || !uid) {
    reject(); //eslint-disable-line
  }

  verify(token, clientId, (error) => {
    if (error) {
      reject(); //eslint-disable-line
    }

    resolve();
  });
});

module.exports = {
  async validateToken(req, res, next) {
    if (!(new RegExp('\\/users\\/auth\\/sign_in').test(req.originalUrl))) {
      const {
        ['access-token']: token, // eslint-disable-line
        client,
        uid
      } = req.headers;

      try {
        await validate(token, client, uid);
      } catch (error) {
        res.status(401).json({
          error: 'Access unauthorized',
        });
        return;
      }
    }
    next();
  },
};
