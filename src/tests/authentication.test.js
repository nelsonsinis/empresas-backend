const request = require('supertest');
const app = require('../config/express');
const {
  version
} = require('../config/config.json');
const mongoose = require('mongoose');

describe('Authentication tests', () => {
  const baseURL = `/api/${version}`;

  afterAll(async () => {
    await mongoose.disconnect();
  });

  test('Sign in', async () => {
    const body = {
      email : 'lucasrizel@ioasys.com.br',
      password : '12345678',
    };

    const response = await request(app).post(`${baseURL}/users/auth/sign_in`).send(body);

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('id');
    expect(response.body).toHaveProperty('email', body.email);
    expect(response.body).not.toHaveProperty('password');
    expect(response.header).toHaveProperty('access-token');
    expect(response.header).toHaveProperty('client');
    expect(response.header).toHaveProperty('uid');
  });
});
