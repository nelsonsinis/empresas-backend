const request = require('supertest');
const app = require('../config/express');
const {
  version
} = require('../config/config.json');
const mongoose = require('mongoose');

describe('Enterprises tests', () => {
  let headers = {};
  const baseURL = `/api/${version}`;

  beforeAll(async () => {
    const body = {
      email: 'lucasrizel@ioasys.com.br',
      password: '12345678',
    };

    const response = await request(app).post(`${baseURL}/users/auth/sign_in`).send(body);

    const {
      ['access-token']: token,
      client,
      uid
    } = response.header;

    headers = {
      token,
      client,
      uid
    };
  });

  afterAll(async () => {
    await mongoose.disconnect();
  });

  it('Enterprise Index and Enterprise Index with filter', async () => {
    const params = {
      enterprise_types: 1,
      name: 'aQm'
    };

    const response = await request(app)
      .get(`${baseURL}/enterprises`)
      .query(params)
      .set('access-token', headers.token)
      .set('uid', headers.uid)
      .set('client', headers.client);

    expect(response.status).toBe(200);
    expect(response.header).toHaveProperty('access-token');
    expect(response.header).toHaveProperty('client');
    expect(response.header).toHaveProperty('uid');
    expect(response.body).toHaveProperty('id');
    expect(response.body.enterprise_type.id).toBe(params.enterprise_types);
    expect(new RegExp(params.name, 'i').test(response.body.enterprise_name)).toBe(true);
  });

  it('Show', async () => {
    const response = await request(app)
      .get(`${baseURL}/enterprises/1`)
      .set('access-token', headers.token)
      .set('uid', headers.uid)
      .set('client', headers.client);

    expect(response.status).toBe(200);
    expect(response.header).toHaveProperty('access-token');
    expect(response.header).toHaveProperty('client');
    expect(response.header).toHaveProperty('uid');
    expect(response.body).toHaveProperty('id');
    expect(response.body.id).toBe(1);
  });
});