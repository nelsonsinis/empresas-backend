const mongoose = require('mongoose');
const {
  database,
  dbUser,
  dbPassword,
  host,
} = require('../../../config/config.json');

module.exports = {
  async connect() {
    try {
      await mongoose.connect(`mongodb://${dbUser}:${dbPassword}@${host}:27017/${database}`, {
        useNewUrlParser: true,
      });
    } catch (error) {
      throw new Error(error);
    }
  },
};
