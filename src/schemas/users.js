const {
  Schema,
  Schema: {
    Types: { Mixed },
  },
} = require('mongoose');

module.exports = new Schema({
  id: {
    type: Number,
    min: 0,
  },
  investor_name: {
    type: String,
  },
  email: {
    type: String,
  },
  city: {
    type: String,
  },
  country: {
    type: String,
  },
  balance: {
    type: Number,
  },
  photo: {
    type: String,
    required: false,
  },
  portfolio: {
    enterprises_number: {
      type: String,
    },
    enterprises: [Mixed],
  },
  portfolio_value: {
    type: Number,
  },
  first_access: {
    type: Boolean,
  },
  super_angel: {
    type: Boolean,
  },
  password: {
    type: String,
  },
}, {
  _id: false,
  collection: 'users',
});
