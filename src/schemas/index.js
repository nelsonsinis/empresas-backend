const users = require('./users');
const enterprises = require('./enterprises');

module.exports = {
  users,
  enterprises,
};
