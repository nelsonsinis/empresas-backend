const {
  Schema,
} = require('mongoose');

module.exports = new Schema({
  id: {
    type: Number,
    min: {
      type: Number,
    },
  },
  enterprise_name: {
    type: String,
  },
  description: {
    type: String,
  },
  email_enterprise: {
    type: String,
  },
  facebook: {
    type: String,
  },
  twitter: {
    type: String,
  },
  linkedin: {
    type: String,
  },
  phone: {
    type: String,
  },
  own_enterprise: {
    type: Boolean,
  },
  photo: {
    type: String,
  },
  value: {
    type: Number,
  },
  shares: {
    type: Number,
  },
  share_price: {
    type: Number,
  },
  own_shares: {
    type: Number,
  },
  city: {
    type: String,
  },
  country: {
    type: String,
  },
  enterprise_type: {
    id: {
      type: Number,
    },
    enterprise_type_name: {
      type: String,
    },
  },
}, {
  collection: 'enterprises',
  _id: false,
});
